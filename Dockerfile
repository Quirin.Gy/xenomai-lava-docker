FROM debian:bullseye-slim

RUN  apt-get -qq update && \
    apt-get install -y jq lavacli

ENTRYPOINT /bin/bash
